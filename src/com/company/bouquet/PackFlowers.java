package com.company.bouquet;

import com.company.flowers.Flower;

import java.util.ArrayList;
import java.util.List;

public class PackFlowers {

    private final List<Flower> flowers = new ArrayList<>();

    public void addFlower(Flower flower) {
        flowers.add(flower);
    }

    public int calculatePackCost() {
        int total = 0;

        for (int i = 0; i < flowers.size(); i++) {
            total += flowers.get(i).getPrice();

            if (flowers.get(i).getPrice() == 0) {
                throw new IllegalArgumentException("The price of flower can't be equal zero!");
                //Exception is checking if any flower have a price equal zero
            }
        }
        System.out.println("The total pack price is: " + total);
        return total;
    }

    public void showPackFlowers() {
        if (flowers.size() == 0) {
            throw new NullPointerException("You should have at least 1 flower in the pack");
            //Exception should be shown if we didn't add any flower to the pack
        } else {
            System.out.println("Pack of flower contains: ");
            showFlowerList(this.flowers);
        }
    }

    private void showFlowerList(List<Flower> a1) {
        for (int i = 0; i < a1.size(); i++) {
            System.out.println(a1.get(i));
        }
    }

}
