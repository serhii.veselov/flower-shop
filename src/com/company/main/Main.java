package com.company.main;

import com.company.bouquet.PackFlowers;
import com.company.flowers.*;
import com.company.flowers.types.Lily;
import com.company.flowers.types.Rose;
import com.company.flowers.types.Tulip;
import com.company.flowers.utilities.FlowerColor;

public class Main {

    public static void main(String[] args) {

        //Here we are creating flowers (setting price and choosing flower's color) which we want to add into a new pack
//        Flower customRose = new Rose(12, FlowerColor.RED);
//        Flower customLily = new Lily(14, FlowerColor.PINK);

        //Creating a new pack
        PackFlowers packFlowers = new PackFlowers();

        //Adding flowers to the pack
//        packFlowers.addFlower(customRose);
//        packFlowers.addFlower(customLily);

        //Generating a list of all the flowers, that we have added to the pack. Printing it in the console.
        packFlowers.showPackFlowers();
        System.out.println();

        //Calculating the total cost of the pack and printing it in the console
        packFlowers.calculatePackCost();
    }

}
