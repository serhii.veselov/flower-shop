package com.company.flowers.utilities;

public enum FlowerColor {

    RED("Red"), WHITE("White"), PINK("Pink"), YELLOW("Yellow");

    private String colorName;

    FlowerColor(String name) {
        this.colorName = name;
    }

    public void getColorName(String name) {
        this.colorName = name;
    }

    public String setColorName() {
        return colorName;
    }

    @Override
    public String toString() {
        return colorName;
    }
}
