package com.company.flowers.utilities;

public enum FlowerName {

    ROSE("Rose"), LILY("Lily"), TULIP("Tulip");

    public String flowerName;

    FlowerName(String name) {
        this.flowerName = name;
    }

    @Override
    public String toString() {
        return flowerName;
    }

}
