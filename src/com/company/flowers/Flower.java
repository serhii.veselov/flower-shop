package com.company.flowers;

import com.company.flowers.utilities.FlowerColor;

public abstract class Flower {

    private final String flowerName;
    private int price;
    private final FlowerColor colorName;

    public Flower(String flowerName, int price, FlowerColor colorName) {
        this.flowerName = flowerName;
        this.price = price;
        this.colorName = colorName;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        if (flowerName.equals("")) {
            throw new NullPointerException("This flower doesn't has a name!");
            //Exception is checking if the flower name doesn't empty
        } else {
            return flowerName + " of " + colorName + " color ";
        }
    }
}
