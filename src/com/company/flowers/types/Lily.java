package com.company.flowers.types;

import com.company.flowers.Flower;
import com.company.flowers.utilities.FlowerColor;

public class Lily extends Flower {

    private final int price;

    public Lily(int price, FlowerColor flowerColor) {
        super("Lily", 0, flowerColor);
        this.price = price;
    }

    @Override
    public int getPrice() {
        return price;
    }

}
