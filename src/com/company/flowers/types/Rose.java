package com.company.flowers.types;

import com.company.flowers.Flower;
import com.company.flowers.utilities.FlowerColor;

public class Rose extends Flower {

    private final int price;

    public Rose(int price, FlowerColor flowerColor) {
        super("Rose", 0, flowerColor);
        this.price = price;
    }

    @Override
    public int getPrice() {
        return price;
    }

}
