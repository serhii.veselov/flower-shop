package com.company.flowers.types;

import com.company.flowers.Flower;
import com.company.flowers.utilities.FlowerColor;

public class Tulip extends Flower {

    private final int price;

    public Tulip(int price, FlowerColor flowerColor) {
        super("Tulip", 0, flowerColor);
        this.price = price;
    }

    @Override
    public int getPrice() {
        return price;
    }

}
